import pandas as pd
from scipy.optimize import minimize
from matplotlib import pyplot as plt
import numpy as np
import random
import xgboost as xgb
from sklearn import preprocessing
def mean_absolute_percentage_error(y_pred, y_true):
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

# load data
dat = pd.read_csv('./BFCom_R1.csv')

# Assume temperature is correct
dat.T_act[np.isnan(dat.T_act)] = dat.T_fcst[np.isnan(dat.T_act)]

# Remove unwanted fields
dat.drop(['date', 'T_fcst', 'TEAMNAME'], inplace=True, axis=1)

# day, month -> date
le = preprocessing.LabelEncoder()
dat['date'] = le.fit_transform(
                    map('-'.join, zip(
                            [str(s) for s in dat.Day.values.tolist()],
                            [str(d) for d in dat.Month.values.tolist()])))
#dat.drop(['Day'], axis=1, inplace=True)

# Training data: what we know
# Test - end resul
train = dat[np.logical_not( np.isnan(dat.Load_MW))]
test = dat[np.isnan(dat.Load_MW)]

target = 'Load_MW'
DTrains = []
DTests = []
modls = []
droplist = [ 
            [target], 
            [target, 'T_act'], 
            #[target, 'Day', 'Month', 'Year', 'date'],
            #[target, 'Day' ]
           ]
preds = []
params={'max_depth':5000, 'min_child_weight':0.4, 'gamma':0}
for dl in droplist:
    DTrains.append(xgb.DMatrix(train.drop(dl, axis=1), train[target]))
    DTests.append(xgb.DMatrix(test.drop(dl, axis=1)))
    modls.append( xgb.train(params, DTrains[-1], 800))
    preds.append(modls[-1].predict(DTests[-1]))


log = open('pred.csv','w')
for pred in sum(preds) / len(preds):
    print>>log, pred
log.close()
