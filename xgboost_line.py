import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import random
import xgboost as xgb
from sklearn import preprocessing
from scipy.optimize import minimize

from sklearn.utils import check_array
def mean_absolute_percentage_error(y_true, y_pred): 
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

# load data
dat = pd.read_csv('./BFCom_R2.csv')

# Use temperature data
#assuming no temps exist > 110
dat.T_fcst[dat.T_fcst > 110] = np.nan
dat.T_act[np.isnan(dat.T_act)] = dat.T_fcst[np.isnan(dat.T_act)]

# Remove unwanted fields
dat.drop(['date', 'T_fcst', 'TEAMNAME'], inplace=True, axis=1)

# day, month -> date
le = preprocessing.LabelEncoder()
dat['date'] = le.fit_transform(
                    map('-'.join, zip(
                            [str(s) for s in dat.Day.values.tolist()],
                            [str(d) for d in dat.Month.values.tolist()])))
dat.drop(['Day'], axis=1, inplace=True)

# Training data: what we know
# Test - end resul
train = dat[np.logical_not( np.isnan(dat.Load_MW))]

#Make polynomial fit to load vs. temp
x=minimize(lambda x: mean_absolute_percentage_error(x[0] * train.T_act**2 + x[1]* train.T_act + x[2], train.Load_MW), [-1,1,2000]).x

dat['fork'] = x[0]*dat.T_act**2 + x[1]*dat.T_act +x[2]

train = dat[np.logical_not( np.isnan(dat.Load_MW))]
test = dat[np.isnan(dat.Load_MW)]

# subset of training data for verification
rows = random.sample(train.index.values, len(train.index.values)/4) # 700 rows, w/e
train_known = train.ix[rows,:].copy(deep=True)
train.drop(rows, inplace=True)

target = 'Load_MW'
DTrain = xgb.DMatrix(train.drop(target, axis=1).drop('fork',axis=1), train[target])
Dtest = xgb.DMatrix(train_known.drop(target, axis=1).drop('fork',axis=1))

modl = xgb.train({'max_depth':5000, 'min_child_weight':0.1, 'gamma':2}, DTrain, 40)
#modl = xgb.train({'max_depth':20, 'eta':0.01},DTrain, 1000)

pred = modl.predict(Dtest)
#for t in np.arange(train_known[target].values.min(), train_known[target].values.max()):
#    sub = train_known[train_known

#dat.res = np.abs( dat.Load_MW - dat.fork)

pred2 = []
weights = np.array([1,6])
weights = weights / float(sum(weights))
for i in range(train_known.shape[0]):
    pred2.append( np.mean([weights[0] * train_known.fork.values[i], weights[1] *  pred[i]]))

print 'errr:',mean_absolute_percentage_error(pred2, train_known[target].values**.5)



#xgb.plot_tree(modl)
#plt.savefig('tree.pdf', dpi=500)


#train_known['se'] = np.abs(np.power(pred,2)- np.power(train_known[target],2))**.5
#plt.plot(train.T_act, train.fork)
#plt.scatter(train_known.T_act, np.abs(train_known.Load_MW.values - pred),c="red")
#plt.scatter(train_known.T_act, pred, color="blue")
#plt.scatter(train_known.T_act, np.abs(train_known.Load_MW.values - pred),c="red") 
#plt.scatter(train_known.T_act, pred, color="blue", alpha=.1)
#plt.scatter(train_known.T_act, train_known.se, c="green", alpha=.1)
#plt.show()



