import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import random
import xgboost as xgb
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error

# load data
dat = pd.read_csv('./BFCom_R1.csv')

# Assume temperature is correct
dat.T_act[np.isnan(dat.T_act)] = dat.T_fcst[np.isnan(dat.T_act)]

# Remove unwanted fields
dat.drop(['date', 'T_fcst', 'TEAMNAME'], inplace=True, axis=1)

# day, month -> date
le = preprocessing.LabelEncoder()
dat['date'] = le.fit_transform(
                    map('-'.join, zip(
                            [str(s) for s in dat.Day.values.tolist()],
                            [str(d) for d in dat.Month.values.tolist()])))
#dat.drop(['Day'], axis=1, inplace=True)

# Training data: what we know
# Test - end resul
train = dat[np.logical_not( np.isnan(dat.Load_MW))]
test = dat[np.isnan(dat.Load_MW)]

# subset of training data for verification
rows = random.sample(train.index.values, len(train.index.values)/4) # 700 rows, w/e
train_known = train.ix[rows,:].copy(deep=True)
train.drop(rows, inplace=True)

target = 'Load_MW'
DTrains = []
DTrain = xgb.DMatrix(train.drop(target, axis=1), train[target])
DTrains.append(DTrain)
DTrain_noT = xgb.DMatrix(train.drop([target, 'T_act'], axis=1), train[target])
DTrains.append(DTrain_noT)
DTrains.append(xgb.DMatrix(train.drop([target, 'T_act', 'Day', 'Month', 'Year', 'date'], axis=1), train[target]))
DTest = xgb.DMatrix(train_known.drop(target, axis=1))
DTest_noT = xgb.DMatrix(train_known.drop(['T_act', target], axis=1))
DTests = [DTest, DTest_noT]
DTests.append( xgb.DMatrix(train_known.drop(['T_act', 'Day', 'Month', 'Year', 
'date', target], axis=1)))

modls = []
modl = xgb.train({'max_depth':5000, 'min_child_weight':0.1, 'gamma':2}, DTrain, 30)
modls.append(modl)
modl_noT = xgb.train({'max_depth':5000, 'min_child_weight':0.1, 'gamma':2}, DTrain_noT, 30)
modls.append(modl_noT)
modls.append( xgb.train({'max_depth':5000, 'min_child_weight':0.1, 'gamma':2}, DTrains[2], 30))
#modl = xgb.train({'max_depth':20, 'eta':0.01},DTrain, 1000)

averages = {} # month-day, 
for month in np.unique(train.Month):
    for day in np.unique(train.Weekday):
        averages[(month,day)] = np.mean( train.Load_MW[np.logical_and(train.Day==day, train.Month==month)])


pred = modl.predict(DTest)
pred2 = modl_noT.predict(DTest_noT)
pred3 = modls[2].predict(DTests[2])
pred4 = test.apply(lambda x : averages[(int(x.Month), int(x.Weekday))],1)
print 'errr:', mean_squared_error( pd.DataFrame([pred,pred2, pred3]).mean(), train_known[target])**.5

#xgb.plot_tree(modl)
#plt.savefig('tree.pdf', dpi=500)
